// BumpInt.c

// Negative logic bump sensors
// P4.7 Bump5, left side of robot
// P4.6 Bump4
// P4.5 Bump3
// P4.3 Bump2
// P4.2 Bump1
// P4.0 Bump0, right side of robot

#include <stdint.h>
#include "msp.h"

// Initialize Bump sensors
// Make six Port 4 pins inputs
// Activate interface pullup
// pins 7,6,5,3,2,0
// Interrupt on falling edge (on touch)
void (*Collision)(uint8_t);

void BumpInt_Init(void(*task)(uint8_t)){
    // write this as part of Lab 14
    P4->SEL0 &= ~0xED;    // 1110 1101
    P4->SEL1 &= ~0xED;    // bump pins as GPIO
    P4->DIR &= ~0xED;    // Make the bump pins in
    P4->REN |= 0xED;     // Enable pull resistors
    P4->OUT |= 0xED;     // Pull up
    P4->IES |= 0xED;     // Make pins a falling edge event
    P4->IFG = ~0xED;     // Clear existing flags
    //P4->IFG = 0x00;     Clear All flag
    P4->IE |= 0xED;     // Arm interrupt on bump pins
        NVIC->IP[9] = (NVIC->IP[9]&0xFF00FFFF)|0x00400000; //highest possible priority and
        NVIC->ISER[1] = 0x00000040;  //enabling interrupt 38 (port 4)
    Collision = task;
}
// Read current state of 6 switches
// Returns a 6-bit positive logic result (0 to 63)
// bit 5 Bump5
// bit 4 Bump4
// bit 3 Bump3
// bit 2 Bump2
// bit 1 Bump1
// bit 0 Bump0
uint8_t Bump_Read(void){
    // write this as part of Lab 14
    uint8_t result = (~P4->IN)&0x01; // 0 bit
    result |= ((~P4->IN) & 0x0C) >>1; // bit 2 3 -> 1 2
    result |= ((~P4->IN) & 0xE0) >>2; // bit 5 6 7 -> 3 4 5
    return result;
}
// we do not care about critical section/race conditions
// triggered on touch, falling edge
void PORT4_IRQHandler(void){
    // write this as part of Lab 14
    // Make the robot stop
    (*Collision)(Bump_Read());
}



#include <stdint.h>
#include "msp.h"
#include "../inc/Clock.h"
#include "../inc/LaunchPad.h"
#include "../inc/Bump.h"
#include "../inc/SysTickInts.h"
#include "../inc/Reflectance.h"
#include "..\inc\CortexM.h"
#include "..\inc\TimerA0.h"
#include "..\inc\PWM.h"
#include "..\inc\MotorSimple.h"

uint8_t timingVar, input, changeState;
int32_t position, offset, rightDutyHolder, leftDutyHolder, delayCycleCompare, delayCycleCurr;

// default Cycles for states
const uint32_t baseCycle = 1750; // 1750
const uint32_t upperMidCycle = 1250; // 1650
const uint32_t lowerMidCycle = 1000; //1500
const uint32_t lowCycle = 0;
const uint32_t extremeCycle = 3500; // 2500
const uint32_t extremeCycleStraight = 2000; // 2000
const uint32_t extremeCycleStraightMore = 3000; // 3000
const uint32_t hardForward = 3500; // 3500
const uint32_t hardBack = 3500; //3000

// offset constants
const uint32_t maxOffset = 250; // 240
const uint32_t offsetIncrease = 25; // 5
const uint32_t offsetIncreaseFast = 150;

// delay constants
const uint32_t shortDelay = 5;
const uint32_t mediumDelay = 5;
const uint32_t longDelay = 2;
const uint32_t delayStraightLong = 400;
const uint32_t delay90Degree = 125;

// Linked data structure
struct State
{
    const uint32_t leftDuty;      // duty cycle left motor
    const uint32_t rightDuty;     // duty cycle right motor
    const uint32_t delay_in_ms;   // state delay
    const uint8_t direction;      // state direction, 1 = straight, 0 = backward
    const struct State *next[12]; // Next if 2-bit input is 0-3
};
typedef const struct State State_t;

#define straight &fsm[0]
#define softLeft &fsm[1]
#define left &fsm[2]
#define hardLeft &fsm[3]
#define left90 &fsm[4]
#define softRight &fsm[5]
#define right &fsm[6]
#define hardRight &fsm[7]
#define right90 &fsm[8]
#define backwards &fsm[9]
#define straightLong &fsm[10]
#define straightLongLong &fsm[11]
// student starter code

State_t fsm[12] = {
    /*0 straight*/ {baseCycle, baseCycle, shortDelay, 0x00, {straight, softLeft, left, hardLeft, left90, softRight, right, hardRight, right90, backwards, straightLong, straightLongLong}},                                              // 0 straight
    /*1 softLeft*/ {upperMidCycle, baseCycle, shortDelay, 0x00, {straight, softLeft, left, hardLeft, left90, softRight, right, hardRight, right90, backwards, straightLong, straightLongLong}},                                          // 1 softLeft
    /*2 left*/ {lowerMidCycle, baseCycle, shortDelay, 0x00, {straight, softLeft, left, hardLeft, left90, softRight, right, hardRight, right90, backwards, straightLong, straightLongLong}},                                              // 2 left
    /*3 hardLeft*/ {hardBack, hardForward, mediumDelay, 0x20, {straight, softLeft, left, hardLeft, left90, softRight, right, hardRight, right90, backwards, straightLong, straightLongLong}},                                            // 3 hardLeft
    /*4 left90*/ {extremeCycle, extremeCycle, delay90Degree, 0x20 /*0x20*/, {straight, softLeft, left, hardLeft, left90, softRight, right, hardRight, right90, backwards, straightLong, straightLongLong}},                              // 4 left90
    /*5 softRight*/ {baseCycle, upperMidCycle, shortDelay, 0x00, {straight, softLeft, left, hardLeft, left90, softRight, right, hardRight, right90, backwards, straightLong, straightLongLong}},                                         // 5 softRight
    /*6 right*/ {baseCycle, lowerMidCycle, shortDelay, 0x00, {straight, softLeft, left, hardLeft, left90, softRight, right, hardRight, right90, backwards, straightLong, straightLongLong}},                                             // 6 right
    /*7 hardRight*/ {hardForward, hardBack, shortDelay, 0x10, {straight, softLeft, left, hardLeft, left90, softRight, right, hardRight, right90, backwards, straightLong, straightLongLong}},                                            // 7 hardRight
    /*8 right90*/ {extremeCycle, extremeCycle, delay90Degree, 0x10 /*0x10*/, {straight, softLeft, left, hardLeft, left90, softRight, right, hardRight, right90, backwards, straightLong, straightLongLong}},                             // 8 right90
    /*9 backwards*/ {extremeCycleStraightMore, extremeCycleStraightMore, delayStraightLong, 0x30, {straight, softLeft, left, hardLeft, left90, softRight, right, hardRight, right90, backwards, straightLong, straightLongLong}},        // 9 backwards
    /*10 straightLong*/ {extremeCycleStraight, extremeCycleStraight, delayStraightLong, 0x00, {straight, softLeft, left, hardLeft, left90, softRight, right, hardRight, right90, backwards, straightLong, straightLongLong}},            // 10 straight long
    /*11 straightLongLong*/ {extremeCycleStraightMore, extremeCycleStraightMore, delayStraightLong, 0x00, {straight, softLeft, left, hardLeft, left90, softRight, right, hardRight, right90, backwards, straightLong, straightLongLong}} // 10 straight long long

};

State_t *Spt;     // pointer to the current state
State_t *prevSpt; // pointer to previous state

void function()
{
}

uint8_t CollisionData, CollisionFlag;  // mailbox
void HandleCollision(uint8_t bumpSensor){
    Motor_StopSimple();
    CollisionData = bumpSensor;
    CollisionFlag = 1;
}

void main(void)
{

    // varialbe initialization
    timingVar = 0, input = 0x18, position = 0, offset = 0;
    rightDutyHolder = 0, leftDutyHolder = 0;
    delayCycleCompare = 0, delayCycleCurr = 0, changeState = 1;

    Clock_Init48MHz();               // running on crystal
    Motor_InitSimple();              // initialize motors
    SysTick_Init(48000, 2);          // set up SysTick for 1000 Hz interrupts
    TimerA0_Init(&function, 5000);   // initialize timer A0 to period of 5000
    PWM_Init12(5000, 3000, 3000, 1); // initialize PWM: period = 5000, both duty = 0, direction = straight
    LaunchPad_Init();                // P1.0 is red LED on LaunchPad
    EnableInterrupts();              // enable interrupt
    BumpInt_Init(&HandleCollision);

    // inititalize to straightLong
    prevSpt = straightLong;
    Spt = straightLong;

    while (1)
    {

        // duty control (duty <= maxDuty
        if (offset > maxOffset)
        {
            offset = maxOffset;
        }

        // set direction of motors
        phase(Spt->direction);

        if (Spt->leftDuty == 0)
        {
            PWM_Duty2(Spt->rightDuty + offset);
        }
        else if (Spt->rightDuty == 0)
        {
            PWM_Duty1(Spt->leftDuty + offset);
        }
        else
        {
            // set duty cycle to the holders
            PWM_Duty1(Spt->leftDuty + offset);
            PWM_Duty2(Spt->rightDuty + offset);
        }

        // set the delay variable
        Clock_Delay1ms(Spt->delay_in_ms);

        prevSpt = Spt;

        if ((input == 0xF0) || (input == 0xE0)|| (input == 0xF8) || (input == 0xFC)){
            Spt = Spt->next[8];
        }
        else if ((input == 0x0F) || (input == 0x07) || (input == 0x1F) || (input == 0x3F)){
            Spt = Spt->next[4];
        }
        else if (input == 0x00)
        { // lost case

            P2->OUT &= ~0x7;

            if ((prevSpt == straight) || (prevSpt == softLeft) || (prevSpt == softRight) || (prevSpt == left) || (prevSpt == right))
            { // if was going straight, keep going
                Spt = Spt->next[0];
            }
            else if ((prevSpt == hardLeft))
            { // if was going left, take left 90
                Spt = Spt->next[4];
            }
            else if ((prevSpt == hardRight))
            { // if was going right, take right 90
                Spt = Spt->next[8];
            }
            else if ((prevSpt == left90))
            { // if took a 90, go straightLong
                Spt = Spt->next[10];
            }
            else if ((prevSpt == right90))
            { // if took a 90, go straightLong
                Spt = Spt->next[10];
            }
            else if ((prevSpt == right90))
            { // if took a 90, go straightLong
                Spt = Spt->next[10];
            }
            else
            { // otherwise, just go back
                Spt = Spt->next[9];
            }
        }
        else if ((position >= -4800) && (position <= 4800))
        { // straight

            P2->OUT &= ~0x7;
            P2->OUT |= 0x0;

            Spt = Spt->next[0];
            if (Spt == prevSpt)
            {
                offset = offset + offsetIncrease;
            }
            else
            {
                offset = 0;
            }
        }
        else if (position < -4800 && position >= -7150)
        { // soft soft turn
            P2->OUT &= ~0x7;
            P2->OUT |= 0x1;

            Spt = Spt->next[1];
            if ((prevSpt == straight) || (prevSpt == softLeft) || (prevSpt == softRight) || (prevSpt == left) || (prevSpt == right))
            {
                offset = offset + offsetIncrease;
            }
            else
            {
                offset = 0;
            }
        }
        else if (position < -7150 && position >= -14300)
        { // soft left

            P2->OUT &= ~0x7;
            P2->OUT |= 0x1;

            Spt = Spt->next[1];
            if ((prevSpt == straight) || (prevSpt == softLeft) || (prevSpt == softRight))
            {
                offset = offset + offsetIncrease;
            }
            else
            {
                offset = 0;
            }
        }
        else if (position < -14300 && position >= -23800)
        { // left

            P2->OUT &= ~0x7;
            P2->OUT |= 0x2;

            Spt = Spt->next[2];
            if ((prevSpt == straight) || (prevSpt == softLeft) || (prevSpt == softRight))
            {
                offset = offset + offsetIncrease;
            }
            else
            {
                offset = 0;
            }
        }
        else if (position <= -23800)
        { // hard left

            P2->OUT &= ~0x7;
            P2->OUT |= 0x2;

            Spt = Spt->next[3];
            if (Spt == prevSpt)
            {
                offset = offset + offsetIncreaseFast;
            }
            else
            {
                offset = 0;
            }
        }
        else if (position > 4800 && position <= 7150)
        { // soft soft right

            P2->OUT &= ~0x7;
            P2->OUT |= 0x3;

            Spt = Spt->next[5];
            if ((prevSpt == straight) || (prevSpt == softLeft) || (prevSpt == softRight))
            {
                offset = offset + offsetIncrease;
            }
            else
            {
                offset = 0;
            }
        }
        else if (position > 7150 && position <= 14300)
            { // soft soft right

                P2->OUT &= ~0x7;
                P2->OUT |= 0x3;

                Spt = Spt->next[5];
                if ((prevSpt == straight) || (prevSpt == softLeft) || (prevSpt == softRight))
                {
                    offset = offset + offsetIncrease;
                }
                else
                {
                    offset = 0;
                }
            }
        else if (position > 14300 && position <= 23800)
        { // right

            P2->OUT &= ~0x7;
            P2->OUT |= 0x4;

            Spt = Spt->next[6];
            if ((prevSpt == straight) || (prevSpt == softLeft) || (prevSpt == softRight))
            {
                offset = offset + offsetIncrease;
            }
            else
            {
                offset = 0;
            }
        }
        else if (position >= 23800)
        { // hard right
            P2->OUT &= ~0x7;
            P2->OUT |= 0x5;

            Spt = Spt->next[7];
            if (Spt == prevSpt)
            {
                offset = offset + offsetIncreaseFast;
            }
            else
            {
                offset = 0;
            }
        }
    }
}

void SysTick_Handler(void)
{ // Systick handler to scan from reflectance sensors

    // timing structure so that
    // I can break up the SysTick
    // Handler into 10 different parts
    if (timingVar == 9)
    {
        timingVar = 0;
    }
    else
    {
        timingVar++;
    }

    // looking at timing variable
    // to decide which function to run
    switch (timingVar)
    {

    case 8:

        Reflectance_Start();
        break;

    case 9:

        input = Reflectance_End();
        position = Reflectance_Position(input);
        break;
    }
}


